<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Resource module version information
 *
 * @package    mod_pdfmagic
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/pdfmagic/lib.php');
require_once($CFG->dirroot.'/mod/pdfmagic/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

global $CFG, $COURSE;

$id       = optional_param('id', 0, PARAM_INT); // Course Module ID
$r        = optional_param('r', 0, PARAM_INT);  // Resource instance ID
$redirect = optional_param('redirect', 0, PARAM_BOOL);
$forceview = optional_param('forceview', 0, PARAM_BOOL);

if ($r) {
    if (!$pdfmagic = $DB->get_record('pdfmagic', array('id'=>$r))) {
        pdfmagic_redirect_if_migrated($r, 0);
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('pdfmagic', $pdfmagic->id, $pdfmagic->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('pdfmagic', $id)) {
        pdfmagic_redirect_if_migrated(0, $id);
        print_error('invalidcoursemodule');
    }
    $pdfmagic = $DB->get_record('pdfmagic', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/pdfmagic:view', $context);


$PAGE->set_url('/mod/pdfmagic/view.php', array('id' => $cm->id));


$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_pdfmagic', 'content', 0, 'sortorder DESC, id DESC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    pdfmagic_print_filenotfound($pdfmagic, $cm, $course);
    die;
} else {
    $PAGE->set_title($course->shortname);
    $PAGE->set_heading($course->fullname);
    echo $OUTPUT->header();
    // Grant editor Teacher 
    $granted_teacher = user_has_role_assignment($USER->id, 3,$DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $COURSE->id))->id);
    
    // Course Manager
    $granted_manager = user_has_role_assignment($USER->id, 1,$DB->get_record('context', array('contextlevel' => CONTEXT_COURSE, 'instanceid' => $COURSE->id))->id);
   
    //System Manager
    $granted_system = is_siteadmin();
    if ($granted_teacher || $granted_manager || $granted_system) {
        pdfmagin_print_button_change_resource();
    }
    $file = reset($files);
    unset($files);
}

$url = moodle_url::make_pluginfile_url(
             $file->get_contextid(),
             $file->get_component(),
             $file->get_filearea(),
             $file->get_itemid(),
             $file->get_filepath(),
             $file->get_filename()
        );

echo "<iframe src = '".$CFG->wwwroot."/mod/pdfmagic/ViewerJS/#".$url."' height='500' width='750' allowfullscreen webkitallowfullscreen></iframe>";
echo $OUTPUT->footer();

//
//
//pdfmagic_display_embed($pdfmagic, $cm, $course, $file);


