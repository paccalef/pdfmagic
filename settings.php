<?php
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
        require_once($CFG->dirroot.'/mod/pdfmagic/lib.php');
        $settings->add(new admin_setting_configtext('pdf_magic_categ', "Choix Categ", "Restreindre à une ou plusieurs catégories. Espace entre les nombres.",""));
}