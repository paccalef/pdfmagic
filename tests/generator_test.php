<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PHPUnit data generator tests.
 *
 * @package mod_pdfmagic
 * @category phpunit
 * @copyright 2013 The Open University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


/**
 * PHPUnit data generator testcase.
 *
 * @package    mod_pdfmagic
 * @category phpunit
 * @copyright 2013 The Open University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_pdfmagic_generator_testcase extends advanced_testcase {
    public function test_generator() {
        global $DB, $SITE;

        $this->resetAfterTest(true);

        // Must be a non-guest user to create pdfmagics.
        $this->setAdminUser();

        // There are 0 pdfmagics initially.
        $this->assertEquals(0, $DB->count_records('pdfmagic'));

        // Create the generator object and do standard checks.
        $generator = $this->getDataGenerator()->get_plugin_generator('mod_pdfmagic');
        $this->assertInstanceOf('mod_pdfmagic_generator', $generator);
        $this->assertEquals('pdfmagic', $generator->get_modulename());

        // Create three instances in the site course.
        $generator->create_instance(array('course' => $SITE->id));
        $generator->create_instance(array('course' => $SITE->id));
        $pdfmagic = $generator->create_instance(array('course' => $SITE->id));
        $this->assertEquals(3, $DB->count_records('pdfmagic'));

        // Check the course-module is correct.
        $cm = get_coursemodule_from_instance('pdfmagic', $pdfmagic->id);
        $this->assertEquals($pdfmagic->id, $cm->instance);
        $this->assertEquals('pdfmagic', $cm->modname);
        $this->assertEquals($SITE->id, $cm->course);

        // Check the context is correct.
        $context = context_module::instance($cm->id);
        $this->assertEquals($pdfmagic->cmid, $context->instanceid);

        // Check that generated pdfmagic module contains a file.
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'mod_pdfmagic', 'content', false, '', false);
        $this->assertEquals(1, count($files));
    }
}
