<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * Merges pdf documents in a course.
 *
 * This file generates a merged pdf document of all the pdf files found in a particular course.
 *
 * @package     tool_mergefiles
 * @copyright   2017 IIT Bombay
 * @author      Kashmira Nagwekar
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/moodlelib.php');
require_once($CFG->libdir . '/filelib.php');

require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/backup/cc/cc_lib/gral_lib/pathutils.php'); // For using function rmdirr(string $dirname).
require_once($CFG->dirroot . '/mod/pdfmagic/perform_merge_form.php');

global $PAGE, $CFG;


// admin_externalpage_setup('toolmergefiles');

//if (empty($id)) {
    $id = required_param('courseid', PARAM_INT);
    //$id = 24;
    $course = get_course($id);
    require_login($course);
  //  $context = context_course::instance($course->id);
    $coursename = format_string($course->fullname, true/*, array('context' => $context)*/);
//}
//require_capability('tool/mergefiles:view', $context);

$url = new moodle_url("/mod/pdfmagic/list_rcs.php", array ('courseid' => $course->id));

// if (empty($id)) {
//     $id = required_param('courseid', PARAM_INT);
//     admin_externalpage_setup('toolmergefiles', '', null, '', array('pagelayout' => 'standard'));
// }

$strlastmodified    = get_string('lastmodified');
$strlocation        = get_string('location');
$coursetitle        = get_string('course');
$strname            = get_string('name');
$strresources       = get_string('resources');
$strsize            = get_string('size');
$strsizeb           = get_string('sizeb');
$strsizemb          = get_string('sizemb');
$pluginname         = get_string('pluginname', 'mod_pdfmagic');

$PAGE->set_url($url);
// $PAGE->set_pagelayout('standard');
$PAGE->set_pagelayout('incourse');
//$PAGE->set_context($context);
$PAGE->set_title($course->shortname . ' | ' . $pluginname);
$PAGE->set_heading($course->fullname . ' | ' . $pluginname);
$PAGE->requires->css("/mod/pdfmagic/styles.css", true);
$PAGE->requires->js_call_amd('mod_pdfmagic/pdf_list_manager', 'init', array());

echo $OUTPUT->header();

// Source code from course/resources.php...used for getting all the pdf files in the course in order to merge them.

// Get list of all resource-like modules.
$allmodules = $DB->get_records('modules', array ('visible' => 1));
$availableresources = array ();
foreach($allmodules as $key => $module) {
    $modname = $module->name;
    $libfile = "$CFG->dirroot/mod/$modname/lib.php";
    if (!file_exists($libfile)) {
        continue;
    }
    $archetype = plugin_supports('mod', $modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
    if ($archetype != MOD_ARCHETYPE_RESOURCE) {
        continue;
    }

    $availableresources[] = $modname; // List of all available resource types.
}
$cms = array ();
$resources = array ();



if (!isset($CFG->pdf_magic_categ) || $CFG->pdf_magic_categ == "") {
    $courses = $DB->get_records_sql("SELECT * FROM {course}");
} else {
    $categ_filter = explode(" ", $CFG->pdf_magic_categ);
    $c_f = "";
    foreach ($categ_filter as $cf) {
        $c_f .= $cf.",";
    }
    $courses = $DB->get_records_sql("SELECT * FROM {course} WHERE category IN (".substr($c_f,0,-1).")");
}
foreach ($courses as $course) {
    $modinfo = get_fast_modinfo($course); // Fetch all course data.
    $usesections = course_format_uses_sections($course->format);

    foreach($modinfo->cms as $cm) { // Fetch all modules in the course, like forum, quiz, resource etc.
        if (!in_array($cm->modname, $availableresources)) {
            continue;
        }
        if (!$cm->uservisible) {
            continue;
        }
        if (!$cm->has_view()) {
            // Exclude label and similar.
            continue;
        }
        $cms[$cm->id] = $cm;
        $resources[$cm->modname][] = $cm->instance; // Fetch only modules having modname -'resource'...
                                                    // ...pdf files have modname 'resource'.
    }
}
// Preload instances.
foreach($resources as $modname => $instances) { // Get data from mdl_resource table, namely, id, name of the pdf file etc.
    $resources[$modname] = $DB->get_records_list($modname, 'id', $instances, 'id', 'id,name');
}

if (!$cms) {
    notice(get_string('thereareno', 'moodle', $strresources), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit();
}

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';
$table->caption = get_string('coursefiles', 'mod_pdfmagic');

if ($usesections) {
    $addbutton = get_string('addbutton', 'mod_pdfmagic');
    $table->head = array (
            $addbutton,
            $strname,
            $coursetitle,
            $strsize);

    $table->align = array (
            'center',
            'left',
            'left',
            'left');
} else {
    $table->head = array (
            $strlastmodified,
            $strname,
            $coursetitle,
            $strsize);

    $table->align = array (
            'left',
            'left',
            'left',
            'left');
}

$fs = get_file_storage();
/*
$tempdir = $CFG->tempdir . '/tool_mergefiles'; // Create temporary storage location for files.
if (!file_exists($tempdir)) {
    $mkdir = mkdir($tempdir, 0777, true);
}
$tempdirpath = $tempdir . '/';
*/
$currentsection = '';
foreach($cms as $cm) {
    // ----------------------------------------------------------------------------
    // Source from mod/resource/view.php.

    $context = context_module::instance($cm->id);
    $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
    if (count($files) < 1) {
        // resource_print_filenotfound($resource, $cm, $course);
        continue;
    } else {
        $file = reset($files);
        unset($files);
    }

    // End of source from mod/resource/view.php.
    // ---------------------------------------------------------------------------
    if (strpos($file->get_filename(),".pdf") !== false) {
        if (!isset($resources[$cm->modname][$cm->instance])) {
            continue;
        }
        $resource = $resources[$cm->modname][$cm->instance];

        $printsection = '';
        if ($usesections) {
            if ($cm->sectionnum !== $currentsection) {
                if ($cm->sectionnum) {
             //       $printsection = get_section_name($course, $cm->sectionnum);
                }
                if ($currentsection !== '') {
                    // $table->data[] = 'hr';
                }
                $currentsection = $cm->sectionnum;
            }
        }

        $extra = empty($cm->extra) ? '' : $cm->extra;
        $icon = '<img src="' . $cm->get_icon_url() . '" class="activityicon" alt="' . $cm->get_module_type_name() . '" /> ';
        $class = $cm->visible ? '' : 'class="dimmed"'; // Hidden modules are dimmed.

        $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());

        static $p = 1;
       /* $file->copy_content_to($tempdirpath . $p . '.pdf');
        $course_files[$p] = $tempdirpath . $p . '.pdf';
*/
        $table->data[] = array (
                "<span class='add' url='".$url."' name='".$file->get_filename()."'>Add</span>",
                "<a $class $extra href=\"" . $url . "\">" . $icon . $cm->get_formatted_name() . "</a>",
                $DB->get_record("course",array("id" => $cm->course))->fullname,
                number_format((($file->get_filesize() / 1000) / 1000), 2) . ' ' . $strsizemb);
        $p++;
    }
}

echo html_writer::table($table);

echo html_writer::tag('div', html_writer::tag('div', "<b> Choosen Items </b> <span id='delete_items'>[Delete]</span>", array("id" => "item_list_selected_title")).html_writer::tag('div', "", array("id" => "item_list_selected_items")), array("id" => "item_list_selected"));

$mform = new perform_merge_form("store_file_merged.php");
$mform->display();


// Merging course files.=========================================================================================

echo $OUTPUT->footer();