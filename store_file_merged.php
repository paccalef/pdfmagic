<?php
require('../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/moodlelib.php');
require_once($CFG->libdir . '/filelib.php');
require_once('lib.php');
require "vendor/autoload.php";

use Clegginabox\PDFMerger;

global $COURSE, $CFG;

$context = context_module::instance($_POST['cmid']);
//$context->id = 70;
$fs = get_file_storage();

$file_system_filedir = new file_system_filedir();
$pdf = new \Clegginabox\PDFMerger\PDFMerger($file_system_filedir, $CFG->dataroot);

foreach (explode("***", $_POST['items']) as $url) {
    if ($url!="") {
        $pdf->addPDF($url, 'all');
    }
}

// Create file containing text 'hello world'
// Prepare file record object
$fileinfo = array(
    'contextid' => $context->id, // ID of context
    'component' => 'mod_pdfmagic'/*'mod_resource'*/,     // usually = table name
    'filearea' => 'content',     // usually = table name
    'itemid' => 0,               // usually = ID of row in table
    'filepath' => '/',           // any path beginning and ending in /
    'filename' => uniqid().'.pdf'); // any filename
$fs->create_file_from_string($fileinfo, $pdf->merge('string', uniqid().'.pdf', $_POST['format']));

$file = $fs->get_file(
                $fileinfo['contextid'],
                $fileinfo['component'],
                $fileinfo['filearea'],
                $fileinfo['itemid'],
                $fileinfo['filepath'],
                $fileinfo['filename']);
  
$url = new moodle_url($CFG->wwwroot.'/mod/pdfmagic/view.php?id='.$_POST['cmid']);
redirect($url);