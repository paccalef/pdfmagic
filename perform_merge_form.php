<?php
require_once("$CFG->libdir/formslib.php");
 
class perform_merge_form extends moodleform {
 
    function definition() {
        global $CFG;
 
        $mform = $this->_form; // Don't forget the underscore! 
 
        $mform->addElement('hidden', 'items');
        $mform->setType('items', PARAM_RAW);
        
        $mform->addElement('hidden', 'cmid', optional_param('id', 0, PARAM_INT));
        $mform->setType('cmid', PARAM_RAW);
        
        $options = array(
            'L' => 'Paysage',
            'P' => 'Portrait'
        );
        $select = $mform->addElement('select', 'format', "Format", $options);
        // This will select the colour blue.
        $select->setSelected('L');
        
        $mform->addElement('submit', 'Submit', 'Submit');
        
    }
}   